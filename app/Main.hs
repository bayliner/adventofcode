module Main where

import Aoc_2020_01
import Aoc_2020_02
import Aoc_2020_03
import Aoc_2020_04
import Aoc_2020_05
import Aoc_2020_06
import Aoc_2020_07
import Aoc_2020_08
import Aoc_2020_09
import Aoc_2020_10
import Aoc_2020_11
import Aoc_2020_12
import Aoc_2020_13
import Aoc_2020_14
import Aoc_2020_15
import Aoc_2020_16
import Aoc_2020_17
import Aoc_2020_18
import Aoc_2020_19
import Aoc_2020_20

main :: IO ()
main = do
  putStrLn "Which AoC challenge should be excecuted: [1-24]"
  number <- read <$> getLine :: IO Int
  case number of
    1 -> do
      input <- readFile "data/01/input.txt"
      run01 input
    2 -> do
      input <- readFile "data/02/input.txt"
      run02 input
    3 -> do
      input <- readFile "data/03/input.txt"
      run03 input
    4 -> do
      input <- readFile "data/04/input.txt"
      run04 input
    5 -> do
      input <- readFile "data/05/input.txt"
      run05 input
    6 -> do
      input <- readFile "data/06/input.txt"
      run06 input
    7 -> do
      input <- readFile "data/07/input.txt"
      run07 input
    8 -> do
      input <- readFile "data/08/input.txt"
      run08 input
    9 -> do
      input <- readFile "data/09/input.txt"
      run09 input
    10 -> do
      input <- readFile "data/10/input.txt"
      run10 input
    11 -> do
      input <- readFile "data/11/input.txt"
      run11 input
    12 -> do
      input <- readFile "data/12/input.txt"
      run12 input
    13 -> do
      input <- readFile "data/13/input.txt"
      run13 input
    14 -> do
      input <- readFile "data/14/input.txt"
      run14 input
    15 -> do
      input <- readFile "data/15/input.txt"
      run15 input
    16 -> do
      input <- readFile "data/16/input.txt"
      run16 input
    17 -> do
      input <- readFile "data/17/input.txt"
      run17 input
    18 -> do
      input <- readFile "data/18/input.txt"
      run18 input
    19 -> do
      input <- readFile "data/19/inputTask2.txt"
      run19 input
    20 -> do
      input <- readFile "data/20/input.txt"
      run20 input
    _ -> putStrLn "Not implemented yet"
