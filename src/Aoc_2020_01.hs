module Aoc_2020_01 where

import qualified Data.IntSet as S

run01 :: String -> IO ()
run01 input = do
  let numbers = map (read :: String -> Int) $ lines input
  case findNumberPair 2020 (S.fromList numbers) numbers of
    Just pair@(a, b) -> do
      putStrLn $ "Number pair: " ++ show pair
      putStrLn $ "Result: " ++ show (a * b)
    Nothing -> putStrLn "No result found"
  case findNumberTriple 2020 (S.fromList numbers) numbers of
    Just triple@(a, b, c) -> do
      putStrLn $ "Number triple: " ++ show triple
      putStrLn $ "Result: " ++ show (a * b * c)
    Nothing -> putStrLn "No result found"

  return ()

findNumberPair :: Int -> S.IntSet -> [Int] -> Maybe (Int, Int)
findNumberPair target m (n : ns)
  | n > target = findNumberPair target m ns
  | (target - n) `S.member` m = Just (n, target - n)
  | otherwise = findNumberPair target m ns
findNumberPair _ _ [] = Nothing

findNumberTriple :: Int -> S.IntSet -> [Int] -> Maybe (Int, Int, Int)
findNumberTriple target m (n : ns)
  | n >= target = findNumberTriple target m ns
  | otherwise = case findNumberPair (target - n) (S.delete n m) (S.toList $ S.delete n m) of
    Just (a, b) -> Just (a, b, n)
    Nothing -> findNumberTriple target m ns
findNumberTriple _ _ [] = Nothing
