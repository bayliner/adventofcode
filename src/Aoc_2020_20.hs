module Aoc_2020_20
  ( run20,
  )
where

import Control.Monad

import Data.Char
import qualified Data.Vector as V

import Prelude hiding ((!!))

import Text.ParserCombinators.ReadP

------ parsing -----
newline :: ReadP Char
newline = char '\n'

decimal :: ReadP Int
decimal = read <$> many1 (satisfy isDigit)

parseTileName :: ReadP Int
parseTileName = do
  string "Tile "
  number <- decimal
  char ':'
  newline
  return number

parseTileLine :: ReadP (V.Vector Bool)
parseTileLine = V.fromList . map ('#' ==) <$> many (char '.' +++ char '#')

parseTile :: ReadP (Int, Grid)
parseTile = do
  number <- parseTileName
  g <- V.fromList <$> parseTileLine `endBy` newline
  return (number, g)

parseInput :: ReadP [(Int, Grid)]
parseInput = parseTile `sepBy` newline

---- Problem solving

rotateLeft :: Grid -> Grid
rotateLeft g = V.imap (\rowIdx col -> V.imap (\colIdx _ -> g !! (colIdx, rows - rowIdx - 1)) col) g
  where
    rows = V.length g

flipAround :: Grid -> Grid
flipAround g = V.imap (\rowIdx col -> V.imap (\colIdx _ -> g !! (rowIdx, rows - colIdx - 1)) col) g
  where
    rows = V.length g

getAllSides :: Grid -> [Grid]
getAllSides g = [
  g,
  rotateLeft g,
  rotateLeft . rotateLeft $ g,
  rotateLeft . rotateLeft . rotateLeft $ g,
  flipAround g,
  flipAround . rotateLeft $ g,
  flipAround . rotateLeft . rotateLeft $ g,
  flipAround . rotateLeft . rotateLeft . rotateLeft $ g
  ]

edgeMatch :: Grid -> Grid -> Bool
edgeMatch g1 g2 = or [a == b | a <- getAllEdges g1, b <- getAllEdges g2]

getAllEdges :: Grid -> [V.Vector Bool]
getAllEdges g =
  let firstCol = V.fromList $ map (\r -> g !! (r, 0)) [0 .. (V.length g -1)]
      lastCol = V.fromList $ map (\r -> g !! (r, V.length g - 1)) [0 .. (V.length g -1)]
   in V.head g : firstCol : lastCol : [V.last g]

hasMatch :: Grid -> Grid -> Bool
hasMatch g1 g2 =
  let g1Sides = getAllSides g1
      g2Sides = getAllSides g2
   in or [edgeMatch g1' g2' | g1' <- g1Sides, g2' <- g2Sides]

findEdges :: [(Int, Grid)] -> [Int]
findEdges nr2Grids =
  do
    let grids = map snd nr2Grids
    let numbers = map fst nr2Grids
    map fst . filter ((==) 2 . snd) . zipWith (\nr matches -> (nr, length $ filter id matches)) numbers $
      map
        ( \g -> do
            [hasMatch g g' | g' <- grids, g /= g']
        )
        grids

type Grid = V.Vector (V.Vector Bool)

(!!) :: Grid -> (Int, Int) -> Bool
(!!) g (c, r) = let col = g V.! c in col V.! r

(!!?) :: Grid -> (Int, Int) -> Maybe Bool
(!!?) g (c, r) = do
  col <- g V.!? c
  col V.!? r

run20 :: String -> IO ()
run20 input = do
  let tiles = fst . head $ filter ((== "") . snd) $ readP_to_S parseInput input
  let edges = findEdges tiles
  print . show . product $ edges
