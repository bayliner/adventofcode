module Aoc_2020_07 where

import Data.Array
import Data.Char (isDigit, isSpace)
import Data.Functor (($>))
import qualified Data.Map as M
import Text.ParserCombinators.ReadP

run07 :: String -> IO ()
run07 input = do
  let parsedBags = fst . head $ filter ((== "") . snd) $ readP_to_S pLines input

  let bagIdsM = M.fromList $ zip (map fst parsedBags) [0..]
  let es = concatMap (\(src, dests) -> [(bagIdsM M.! src, ann, bagIdsM M.! dest) | (dest, ann) <- dests]) parsedBags
--  print $ "Bag ids: " ++ show bagIdsM
--  print $ "Edges: " ++ show edges
  let graph = buildG (0, M.size bagIdsM-1) es
  let reachableFromShiny = getReachableNodes (bagIdsM M.! "shinygold") graph
  let reachable = length . filter id $ elems reachableFromShiny
  print $ "Part1: " ++ show (reachable-1)
  -- part 2
  let countBagsFromShiny = countBags (bagIdsM M.! "shinygold") graph
  print $ "Part2: " ++ show (countBagsFromShiny - 1)


getReachableNodes :: Vertex -> Graph Int -> Table Bool
getReachableNodes start = foldGAllImplementation False reachable
    where reachable v' neighbours
            | v' == start = True
            | otherwise = any snd neighbours

countBags :: Vertex -> Graph Int -> Int
countBags v gr
  | null (gr!v)  = 1
  | otherwise = 1 + sum (map (\(ann, v') -> ann * countBags v' gr) (gr!v))


type BagName = String

-- | Graph data structures
type Vertex = Int
type Table a = Array Vertex a
type Graph e = Table [(e, Vertex)]
type Bounds  = (Vertex, Vertex)
type Edge e = (Vertex, e, Vertex)

-- |  Graph functions (see https://wiki.haskell.org/The_Monad.Reader/Issue5/Practical_Graph_Handling#Shortest_Path)
-- | Build a graph from a list of edges.
buildG :: Bounds -> [Edge e] -> Graph e
buildG bounds0 edges0 = accumArray (flip (:)) [] bounds0 [(v, (l,w)) | (v,l,w) <- edges0]

edges :: Graph e -> [Edge e]
edges g = [ (v, l, w) | v <- indices g, (l, w) <- g!v ]

foldGAllImplementation :: (Eq r) => r -> (Vertex -> [(a, r)] -> r) -> Graph a -> Table r
foldGAllImplementation bot f gr = finalTbl
    where finalTbl = fixedPoint updateTbl initialTbl
          initialTbl = listArray bnds (replicate (rangeSize bnds) bot)

          fixedPoint f' x = fp x
              where fp z = if z == z' then z else fp z'
                        where z' = f' z
          updateTbl tbl = listArray bnds $ map recompute $ indices gr
              where recompute v = f v [(b, tbl!k) | (b, k) <- gr!v]
          bnds = bounds gr


-- parse input
pLines :: ReadP [(BagName, [(BagName, Int)])]
pLines = pLine `endBy` char '\n'

pLine :: ReadP (BagName, [(BagName, Int)])
pLine = do
  parent <- parentBag
  childs <- childBags
  return (parent, childs)

parentBag :: ReadP BagName
parentBag = concat <$> manyTill (letters <* whitespace) (string "bags contain")

childBags :: ReadP [(BagName, Int)]
childBags = do
  skipSpaces
  (childBag `endBy` (char ',' +++ char '.' )) +++ (string "no other bags." $> [])

childBag :: ReadP (BagName, Int)
childBag = do
  skipSpaces
  amount <- decimal
  skipSpaces
  name <- concat <$> manyTill (letters <* whitespace) (string "bags" +++ string "bag")
  return (name, amount)

whitespace :: ReadP Char
whitespace = char ' '

decimal :: ReadP Int
decimal = read <$> many1 (satisfy isDigit)

letters :: ReadP String
letters = many1 (satisfy (not . isSpace))
