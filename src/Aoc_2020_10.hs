module Aoc_2020_10 (run10) where

import Data.List (sort)

run10 :: String -> IO ()
run10 input = do
  let jolts = (\l -> 0:l ++ [last l + 3]) . sort . map read $ lines input :: [Int]
  let differences = countDifferences jolts
  print $ "Task 1: " ++ show (length (filter (== 3) differences) * length (filter (== 1) differences))
  let arrangements = countArrangements (getSubLists jolts)
  print $ "Task 2: " ++ show arrangements

countDifferences :: [Int] -> [Int]
countDifferences [] = []
countDifferences [_] = []
countDifferences (x:y:rest) = (y-x) : countDifferences (y:rest)

countArrangements :: [[Int]] -> Int
countArrangements [] = 0
countArrangements [a] = computeMultiplicator a
countArrangements (a:b:cs)
     | (head b - last a) == 3 = let multiplicator = computeMultiplicator a in
           multiplicator * countArrangements (b:cs)
     | otherwise =  1 + computeMultiplicator a + countArrangements (b:cs)

computeMultiplicator :: [Int] -> Int
computeMultiplicator xs
      | length xs <= 2 = 1
      | length xs == 3 = 2
      | otherwise = 1 + ((length xs- 3) * 3)

getSubLists :: [Int] -> [[Int]]
getSubLists [] = []
getSubLists [a] = [[a]]
getSubLists l = sublist : getSubLists (drop (length sublist) l)
              where sublist = getSubList l
getSubList :: [Int] -> [Int]
getSubList [] = []
getSubList [x] = [x]
getSubList (a:b:xs)
      | b - a  > 1 = [a]
      | otherwise = a: getSubList (b:xs)
