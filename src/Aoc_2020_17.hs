module Aoc_2020_17 (run17) where

import Data.Maybe
import qualified Data.Vector as V

parseInput :: String -> [[Bool]]
parseInput input =
  let inputSpace = (map . map) ('#' ==) . lines $ input
      maxWidthSpace = map (\row -> replicate 6 False ++ row ++ replicate 6 False) inputSpace
      width = length . head $ maxWidthSpace
      emptyRow = replicate width False
   in replicate 6 emptyRow ++ maxWidthSpace ++ replicate 6 emptyRow


type Grid = V.Vector (V.Vector Bool)
type Space = V.Vector Grid
type HyperSpace = V.Vector Space

(!!!?) :: Space -> (Int, Int, Int) -> Maybe Bool
(!!!?) g (p, c, r) = do
  plain <- g V.!? p
  col <- plain V.!? c
  col V.!? r

(!!!!?) :: HyperSpace -> (Int, Int, Int,Int) -> Maybe Bool
(!!!!?) g (h,p, c, r) = do
  space <- g V.!? h
  plain <- space V.!? p
  col <- plain V.!? c
  col V.!? r

getNeighbours :: (Int, Int, Int) -> [(Int, Int, Int)]
getNeighbours (z, x, y) = filter ((z, x, y) /=) [(dz + z, dx + x, dy + y) | dz <- [-1, 0, 1], dx <- [-1, 0, 1], dy <- [-1, 0, 1]]

get4DNeighbours :: (Int, Int, Int, Int) -> [(Int, Int, Int,Int)]
get4DNeighbours (w, z, x, y) = filter ((w, z, x, y) /=) [(dw + w ,dz + z, dx + x, dy + y) | dw <- [-1,0,1], dz <- [-1, 0, 1], dx <- [-1, 0, 1], dy <- [-1, 0, 1]]

grid :: [[Bool]] -> Grid
grid res = V.fromList (map V.fromList res)

createSpace :: [[Bool]] -> Space
createSpace inputGrid =
  let emptyGrid = (map . map) (const False) inputGrid
      dimensions = replicate 6 emptyGrid ++ [inputGrid] ++ replicate 6 emptyGrid
   in V.fromList (map grid dimensions)

createHyperSpace :: Space -> HyperSpace
createHyperSpace space =
  let emptySpace = (V.map . V.map . V.map) (const False) space
      dimensions = replicate 6 emptySpace ++ [space] ++ replicate 6 emptySpace
   in V.fromList dimensions

updateCube :: Space -> Bool -> (Int, Int, Int) -> Bool
updateCube s current coord =
  let neighbourValues = mapMaybe (s !!!?) $ getNeighbours coord
   in if current
        then (length . filter id $ neighbourValues) == 2 || (length . filter id $ neighbourValues) == 3
        else (length . filter id $ neighbourValues) == 3

updateHyperCube :: HyperSpace -> Bool -> (Int, Int, Int, Int) -> Bool
updateHyperCube s current coord =
  let neighbourValues = mapMaybe (s !!!!?) $ get4DNeighbours coord
   in if current
        then (length . filter id $ neighbourValues) == 2 || (length . filter id $ neighbourValues) == 3
        else (length . filter id $ neighbourValues) == 3

gameOfLifeRound :: Space -> Space
gameOfLifeRound s = V.imap (\z grids -> V.imap (\x rows -> V.imap (\y current -> updateCube s current (z, x, y)) rows) grids) s


gameOfLifeHyperRound :: HyperSpace -> HyperSpace
gameOfLifeHyperRound h = V.imap (\w spaces -> V.imap (\z grids -> V.imap (\x rows -> V.imap (\y current -> updateHyperCube h current (w,z, x, y)) rows) grids) spaces) h

gameOfLife :: Space -> Space
gameOfLife s = (!! 6) $ iterate gameOfLifeRound s

gameOfLife4D :: HyperSpace -> HyperSpace
gameOfLife4D h = (!! 6) $ iterate gameOfLifeHyperRound h

countActives :: Space -> Int
countActives = V.foldr (\grids acc -> acc + V.foldr (\grid acc2 -> acc2 + (length . filter id $ V.toList grid)) 0 grids) 0

countHyperActives :: HyperSpace -> Int
countHyperActives = V.foldr(\spaces acc0 -> acc0 + V.foldr (\grids acc -> acc + V.foldr (\grid acc2 -> acc2 + (length . filter id $ V.toList grid)) 0 grids) 0 spaces) 0

task1 :: Space -> Int
task1 = countActives . gameOfLife

task2 :: HyperSpace -> Int
task2 = countHyperActives . gameOfLife4D

run17 :: String -> IO ()
run17 input = do
  let space = createSpace . parseInput $ input
  let hyperSpace = createHyperSpace . createSpace . parseInput $ input
  print $ "Task1 " ++ show (task1 space)
  print $ "Task2 " ++ show (task2 hyperSpace)
  return ()
