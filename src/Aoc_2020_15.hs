{-# LANGUAGE TypeApplications #-}

module Aoc_2020_15 (run15) where

import Control.Monad
import Control.Monad.ST.Lazy
import Data.IntMap as I
import Data.List.Split
import Data.STRef.Lazy

import Debug.Trace (trace)

run15 :: String -> IO ()
run15 input = do
  let numbers = Prelude.map (read @Int) $ splitOn "," input
  print $ show numbers
  print $ show (task1 numbers (30000000 - (length numbers + 1)))
  return ()

task1 :: [Int] -> Int -> Int
task1 numbers count =
  runST
    ( do
        refMap <- newSTRef I.empty
        forM_
          (zip [1 ..] (init numbers))
          ( \(turn, n) -> do
              iMap <- readSTRef refMap
              writeSTRef refMap (I.insert n turn iMap)
          )

        fst <$> foldM
          ( \(current, turn) _ -> do
              iMap <- readSTRef refMap
              case iMap I.!? current of
                Nothing -> do
                  writeSTRef refMap (I.insert current turn iMap)
                  return (0, turn + 1)
                Just v -> do
                  let difference = turn - v
                  writeSTRef refMap (I.insert current turn iMap)
                  return (difference, turn + 1)
          )
          (last numbers, length numbers)
          [0 .. count]
    )
