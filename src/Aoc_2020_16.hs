module Aoc_2020_16 (run16) where

import Data.Bifunctor (second)
import Data.Char (isAlphaNum, isDigit)
import Data.Range
import Text.ParserCombinators.ReadP hiding (get)

newline :: ReadP Char
newline = char '\n'

decimal :: ReadP Int
decimal = read <$> many1 (satisfy isDigit)

asciis :: ReadP String
asciis = many (satisfy isAlphaNum)

parseRange :: ReadP (Range Int)
parseRange = do
  from <- decimal
  char '-'
  to <- decimal
  return $ from +=+ to

parseTicketField :: String -> ReadP (Range Int, Range Int)
parseTicketField name = do
  string (name ++ ": ")
  first <- parseRange
  string " or "
  second <- parseRange
  newline
  return (first, second)

parseTicket :: ReadP Ticket
parseTicket =
  Ticket <$> parseTicketField "departure location"
    <*> parseTicketField "departure station"
    <*> parseTicketField "departure platform"
    <*> parseTicketField "departure track"
    <*> parseTicketField "departure date"
    <*> parseTicketField "departure time"
    <*> parseTicketField "arrival location"
    <*> parseTicketField "arrival station"
    <*> parseTicketField "arrival platform"
    <*> parseTicketField "arrival track"
    <*> parseTicketField "class"
    <*> parseTicketField "duration"
    <*> parseTicketField "price"
    <*> parseTicketField "route"
    <*> parseTicketField "row"
    <*> parseTicketField "seat"
    <*> parseTicketField "train"
    <*> parseTicketField "type"
    <*> parseTicketField "wagon"
    <*> parseTicketField "zone"

parseTicketNumbers :: ReadP [Int]
parseTicketNumbers = decimal `sepBy` char ','

parseInput :: ReadP (Ticket, [Int], [[Int]])
parseInput = do
  t <- parseTicket
  myTicket <- newline *> string "your ticket:" *> newline *> parseTicketNumbers
  count 2 newline *> string "nearby tickets:" *> newline
  testTickets <- parseTicketNumbers `endBy` newline
  return (t, myTicket, testTickets)

data Ticket = Ticket
  { dep_loc   :: (Range Int, Range Int),
    dep_stat  :: (Range Int, Range Int),
    dep_plat  :: (Range Int, Range Int),
    dep_track :: (Range Int, Range Int),
    dep_date  :: (Range Int, Range Int),
    dep_time  :: (Range Int, Range Int),
    arr_loc   :: (Range Int, Range Int),
    arr_stat  :: (Range Int, Range Int),
    arr_plat  :: (Range Int, Range Int),
    arr_track :: (Range Int, Range Int),
    cl        :: (Range Int, Range Int),
    duration  :: (Range Int, Range Int),
    price     :: (Range Int, Range Int),
    route     :: (Range Int, Range Int),
    row       :: (Range Int, Range Int),
    seat      :: (Range Int, Range Int),
    train     :: (Range Int, Range Int),
    ty        :: (Range Int, Range Int),
    wagon     :: (Range Int, Range Int),
    zone      :: (Range Int, Range Int)
  }
  deriving (Show)

isTicketInvalid :: Ticket -> [Int] -> Bool
isTicketInvalid t =
  foldr
    ( \x acc ->
        case acc of
          True -> acc
          False ->
            all
              ( \f ->
                  let (r_first, r_last) = (snd f) t
                   in not (inRange r_first x) && not (inRange r_last x)
              )
              ranges
    )
    False

findFieldIndices :: Ticket -> [Int] -> [Int]
findFieldIndices t xs =
 map fst . filter ((==) True . snd). zip [0..] $
  map
    ( \f ->
        all
          ( \x ->
              let (r_first, r_last) = (snd f) t
               in inRange r_first x || inRange r_last x
          )
          xs
    )
    ranges

ranges :: [(String, Ticket -> (Range Int, Range Int))]
ranges =
  [ ("dep_loc", dep_loc),
    ("dep_stat", dep_stat),
    ("dep_plat", dep_plat),
    ("dep_track", dep_track),
    ("dep_date", dep_date),
    ("dep_time", dep_time),
    ("arr_loc", arr_loc),
    ("arr_stat", arr_stat),
    ("arr_plat", arr_plat),
    ("arr_track", arr_track),
    ("cl", cl),
    ("duration", duration),
    ("price", price),
    ("route", route),
    ("row", row),
    ("seat", seat),
    ("train", train),
    ("ty", ty),
    ("wagon", wagon),
    ("zone", zone)
  ]

validTickets :: Ticket -> [[Int]] -> [[Int]]
validTickets t = filter (not . isTicketInvalid t)

fieldToIndices :: Ticket -> [[Int]] -> [[Int]]
fieldToIndices t xs = let valids = validTickets t xs in
                  map (\n -> findFieldIndices t (map (!! n) valids)) [0..19]

findTicketMappings :: [(Int, [Int])] -> [(Int, String)]
findTicketMappings possibleFields
        | length (filter id . map (null . snd) $ possibleFields) == length possibleFields = []
        | otherwise =
         let solvedFields = filter ((==) 1 . length . snd) possibleFields
             fieldsToRemove = map (head . snd) solvedFields
             indToFieldName = map (\s -> (fst s, fst (ranges !! head (snd s)))) solvedFields
          in indToFieldName ++ findTicketMappings (map (second (filter (`notElem` fieldsToRemove)) ) possibleFields)

task2 :: Ticket -> [[Int]] -> [Int] -> Int
task2 t xs my = let finalMapping = findTicketMappings $ zip [0..] (fieldToIndices t xs)
                    relevantIdx = map fst . filter ( flip elem relevantFields . snd) $ finalMapping
                 in product $ map (my !!) relevantIdx

relevantFields :: [String]
relevantFields =
  [ "dep_loc",
    "dep_stat",
    "dep_plat",
    "dep_track",
    "dep_date",
    "dep_time"]

run16 :: String -> IO ()
run16 input = do
  let (ticket, my, tests) = fst . head $ filter ((== "") . snd) $ readP_to_S parseInput input
  print $ "Task2: " ++ show (task2 ticket tests my)
  return ()
