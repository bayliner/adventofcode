module Aoc_2020_08 where

import Data.Char (isDigit)
import qualified Data.IntMap.Strict as M
import qualified Data.IntSet as S
import Text.ParserCombinators.ReadP

run08 :: String -> IO ()
run08 input = do
  let instructions = fst . head $ filter ((== "") . snd) $ readP_to_S inputP input
  let instructionMap = createInstructionMap instructions
  let akkInfiniteLoop = runProgram instructionMap 0 S.empty 0
  print $ "Task1: " ++ show akkInfiniteLoop
  let successfullRuns = filter ((==) Successful . snd) $ map (\program -> runProgram (createInstructionMap program) 0 S.empty 0) (getRunPermutations instructions)
  print $ "Task2: " ++ show successfullRuns

createInstructionMap :: [a] -> M.IntMap a
createInstructionMap = M.fromList . zip [0 ..]

data Instruction = JMP Int | ACC Int | NOP Int deriving (Show, Eq)

data ProgramResult = Successful | NotSuccessful deriving (Show, Eq)

type Position = Int

type Accumulator = Int

runProgram :: M.IntMap Instruction -> Position -> S.IntSet -> Accumulator -> (Accumulator, ProgramResult)
runProgram m p s akk
  | M.size m <= p = (akk, Successful)
  | otherwise =
    if S.member p s
      then (akk, NotSuccessful)
      else case m M.! p of
        JMP p' -> runProgram m (p' + p) (S.insert p s) akk
        ACC a' -> runProgram m (p + 1) (S.insert p s) (akk + a')
        NOP _ -> runProgram m (p + 1) (S.insert p s) akk

getRunPermutations :: [Instruction] -> [[Instruction]]
getRunPermutations (a@(ACC _) : xs) = map (a :) (getRunPermutations xs)
getRunPermutations (j@(JMP v) : xs) = (NOP v : xs) : map (j :) (getRunPermutations xs)
getRunPermutations (n@(NOP v) : xs) = (JMP v : xs) : map (n :) (getRunPermutations xs)
getRunPermutations [] = []

-- parse data
inputP :: ReadP [Instruction]
inputP = lineP `endBy` newline

lineP :: ReadP Instruction
lineP = jmpP +++ accP +++ nopP

jmpP :: ReadP Instruction
jmpP = do
  string "jmp"
  whitespace
  JMP <$> (positiveNumber +++ negativeNumber)

accP :: ReadP Instruction
accP = do
  string "acc"
  whitespace
  ACC <$> (positiveNumber +++ negativeNumber)

nopP :: ReadP Instruction
nopP = do
  string "nop"
  whitespace
  NOP <$> negativeNumber +++ positiveNumber

positiveNumber :: ReadP Int
positiveNumber = do
  char '+'
  decimal

negativeNumber :: ReadP Int
negativeNumber = do
  char '-'
  (* (-1)) <$> decimal

newline :: ReadP Char
newline = char '\n'

whitespace :: ReadP Char
whitespace = char ' '

decimal :: ReadP Int
decimal = read <$> many1 (satisfy isDigit)
