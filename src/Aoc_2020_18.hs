{-# LANGUAGE OverloadedStrings #-}

module Aoc_2020_18 (run18) where

import Control.Monad
import Control.Monad.Combinators.Expr
import Data.Void
import Data.Either
import Text.Megaparsec
import qualified Text.Megaparsec.Char as C
import qualified Text.Megaparsec.Char.Lexer as L

type Parser = Parsec Void String

sc :: Parser ()
sc = L.space (void C.spaceChar) lineComment blockComment
  where
    lineComment = L.skipLineComment "//"
    blockComment = L.skipBlockComment "/*" "*/"

symbol :: String -> Parser String
symbol = L.symbol sc

lexeme :: Parser a -> Parser a
lexeme = L.lexeme sc

parens :: Parser a -> Parser a
parens = between (symbol "(") (symbol ")")

binary  name f = InfixL  (f <$ symbol name)

pInteger :: Parser Int
pInteger = lexeme L.decimal

pTerm :: Parser Int
pTerm = choice
  [ parens pExpr
  , pInteger
  ]

pExpr :: Parser Int
pExpr = makeExprParser pTerm ptable

-- Change this table to receive the result for task1.
-- Task1:
-- ptable = [ [ binary  "+"  (+), binary  "*"  (*) ]]
ptable = [ [ binary  "+"  (+)] , [binary  "*"  (*) ]]

parseInput :: String -> Either (ParseErrorBundle String Void) Int
parseInput = parse (pExpr <* eof) ""

run18 :: String -> IO ()
run18 input = do
  let task2 = sum . rights . map parseInput $ lines input
  putStrLn $ "Task2: " ++ show task2
