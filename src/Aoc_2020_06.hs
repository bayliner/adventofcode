module Aoc_2020_06 where

import Data.List.Split (splitWhen)
import Data.List (nub)


run06 :: String -> IO ()
run06 input = do
  let parsedInput = splitWhen null $ lines input

  -- Part 1: Answers that have been selected by anyone
  let resultAny = foldr (\e akk -> akk + (length . nub . concat $ e)) 0 parsedInput
  print $ "Exercise1: " ++ show resultAny

  -- Part 2: Answers that have been selected by everyone
  let answersEvery = map (\c -> foldr (\e akk -> filter (`elem` akk) e) (head c) (tail c)) parsedInput
  print $ "Exercise2: " ++ show (foldr (\e akk -> akk + length e) 0 answersEvery)
  return ()
