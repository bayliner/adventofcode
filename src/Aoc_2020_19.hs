module Aoc_2020_19
    ( run19
    ) where

import Control.Monad
import Data.Char (isAlphaNum, isDigit)
import qualified Data.IntMap as I
import Text.ParserCombinators.ReadP
import qualified Data.List.Safe as S
import Data.Maybe

------ parsing -----
newline :: ReadP Char
newline = char '\n'

whitespace :: ReadP Char
whitespace = char ' '

decimal :: ReadP Int
decimal = read <$> many1 (satisfy isDigit)

asciis :: ReadP String
asciis = many (satisfy isAlphaNum)

parseInput :: ReadP (I.IntMap Rule, [String])
parseInput = do
  r <- parseRules
  newline
  tests <- parseTests
  return (r, tests)

parseTests :: ReadP [String]
parseTests = asciis `sepBy` newline

parseRules :: ReadP (I.IntMap Rule)
parseRules = I.fromList <$> (parseRule `endBy` newline)

parseRule :: ReadP (Int, Rule)
parseRule = do
  number <- decimal
  char ':'
  whitespace
  rule <- parseOr +++ parseAnd +++ (Literal <$> between (char '"') (char '"') get)
  return (number, rule)

parseOr :: ReadP Rule
parseOr = do
   (a:b:_) <- parseAnd `sepBy` string " | "
   return $ Or a b

parseAnd :: ReadP Rule
parseAnd = do
  And <$> (See <$> decimal) `sepBy` whitespace

----- check rules ----
validateRule :: I.IntMap Rule -> Rule -> ReadP ()
validateRule m (Literal c) = void (char c)
validateRule m (See x) = validateRule m (m I.! x)
validateRule m (Or a b) = validateRule m a +++ validateRule m b
validateRule m (And xs) = mapM_ (validateRule m) xs


validateAllRules :: I.IntMap Rule -> [String] -> Int
validateAllRules m = length . mapMaybe (S.head . filter ((== "") . snd) . readP_to_S (validateRule m (See 0)))

data Rule =
   Literal Char |
   See Int |
   And [Rule]   |
   Or Rule Rule deriving (Show)

run19 :: String -> IO ()
run19 input = do
  let foo = uncurry validateAllRules . fst . head $ filter ((== "") . snd) $ readP_to_S parseInput input
  putStrLn $ "Task 1: " ++ show foo
