module Aoc_2020_09 (run09) where

import Data.List (sort)

run09 :: String -> IO ()
run09 input = do
  let numbers = map read $ lines input :: [Int]
  let ranges = zip [0..] (drop praeambleLength numbers)
  let validateXMAS =
       map (\(start, numberToCheck) -> (isValidXMAS numberToCheck (getSubList praeambleLength start numbers), numberToCheck)) ranges
  let invalidNumber = snd . head . filter (not . fst) $ validateXMAS
  print $ "Task1: " ++ show invalidNumber
  let contSet = sort $ findContiguousSet invalidNumber numbers
  let weakness = head contSet + last contSet
  print $ "Task2: " ++ show weakness


praeambleLength :: Int
praeambleLength = 25

findContiguousSet :: Int -> [Int] -> [Int]
findContiguousSet _ []  = []
findContiguousSet target list@(_:xs) = let contSet = sumUpFromCurrent
                                in if last contSet == target then take (length contSet - 1) list
                                   else findContiguousSet target xs
     where sumUpFromCurrent = takeWhile (<= target) $ scanl (+) 0 list

getSubList :: Int -> Int -> [Int] -> [Int]
getSubList len start l = take len (drop start l)

isValidXMAS :: Int -> [Int] -> Bool
isValidXMAS _ [] = False
isValidXMAS a (x:xs)
    | a-x `elem` xs = True
    | otherwise = isValidXMAS a xs
