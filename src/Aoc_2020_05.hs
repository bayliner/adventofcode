module Aoc_2020_05 where

import Data.List (maximum)
import Data.Array.Base
import Data.Array.ST
import Control.Monad.ST

run05 :: String -> IO ()
run05 input = do
  let binSeats = lines input
  let seatIds = map computeSeatIds binSeats
  print $ "Part 1: Highest seat id: " ++ show (maximum seatIds)
  let coords = map computeRowAndCol binSeats
  let unusedSeats = filter ((== False) . snd ) . assocs $ runArray coords
  let yourUnusedSeat = fst . head $ filter ((\d -> d >= 4 && d <= 113) . fst . fst ) unusedSeats
  print $ "Part 2: Your seat id: " ++ show (computeSeatId yourUnusedSeat)

computeSeatIds :: String -> Int
computeSeatIds code = computeSeatId $ computeRowAndCol code

computeSeatId :: (Int, Int) -> Int
computeSeatId = (\(r, c) -> r * 8 + c)

computeRowAndCol :: String -> (Int, Int)
computeRowAndCol code =  (computeRow (take 7 code), computeCol (drop 7 code))

runArray :: [(Int, Int)] -> UArray (Int, Int) Bool
runArray coords = runSTUArray $
  do arr <- createArray (1,0) (120,7)
     insertElements arr coords
     return arr

createArray :: (Int, Int) -> (Int, Int) -> ST s (STUArray s (Int,Int) Bool)
createArray begin end = do
  newArray (begin,end) False

insertElements :: STUArray s (Int, Int) Bool -> [(Int, Int)] -> ST s ()
insertElements arr = mapM_ (\idx -> writeArray arr idx True)

computeExistingSeatId :: String -> Int
computeExistingSeatId code
  | row >= 120 || row <= 7 = 0
  | otherwise = row * 8 + computeCol (drop 7 code)
  where row = computeRow (take 7 code)

computeRow :: String -> Int
computeRow = fst . foldr (\c (v,b) -> if c =='F' then (v,b*2) else (v+b, b*2) ) (0, 1)

computeCol :: String -> Int
computeCol = fst . foldr (\c (v,b) -> if c =='L' then (v,b*2) else (v+b, b*2) ) (0, 1)
