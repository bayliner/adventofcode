module Aoc_2020_11 (run11) where

import qualified Data.Vector as V
import Data.List (find)
import Data.Maybe
import Data.Functor
import Text.ParserCombinators.ReadP

type Grid = V.Vector (V.Vector RoomEntity)

data RoomEntity = EmptySeat | UsedSeat | Floor deriving (Eq)

instance Show (RoomEntity) where
  show p
        | p == UsedSeat = "#"
        | p ==  EmptySeat = "L"
        | otherwise = "."

run11 :: String -> IO ()
run11 input = do
  let initialRoom = grid . fst . head $ filter ((== "") . snd) $ readP_to_S parseRoom input
  let finalSeats1 = fixpoint initialRoom getAdjNeighbours
  print $ "Task 1: " ++ show finalSeats1
  let finalSeats2 = fixpoint initialRoom getVisibleNeighbours
  print $ "Task 2: " ++ show finalSeats2
  return ()

grid :: [[RoomEntity]] -> Grid
grid res = V.fromList (map V.fromList res)

(!!?) :: Grid -> (Int, Int) -> Maybe RoomEntity
(!!?) g (c,r) = do
  col <- g V.!? c
  col V.!? r
  
getAdjNeighbours :: (Int, Int) -> Grid -> [RoomEntity]
getAdjNeighbours pos@(row,col) g = do
      let adjs = filter (pos /=) [(row+mx, col+my) |mx <- [-1,0,1], my <- [-1,0,1]]
      mapMaybe (g !!?) adjs

getVisibleNeighbours :: (Int, Int) -> Grid -> [RoomEntity]
getVisibleNeighbours pos@(row,col) g = do
    let directions =  filter (/= (0,0)) [(mx, my) |mx <- [-1,0,1], my <- [-1,0,1]]
    catMaybes $ mapMaybe (\(dx,dy) -> find (/= Just Floor) $ map (\n ->  g !!? (row + dx*n, col + dy*n))[1..]) directions


seatNextRound :: (Int, Int) -> Grid -> ((Int,Int) -> Grid -> [RoomEntity]) -> RoomEntity
seatNextRound pos g neighbourComputation = case currentEntity of
       UsedSeat -> if length (filter (UsedSeat ==) neighbours) > 4  then EmptySeat else UsedSeat
       EmptySeat -> if UsedSeat `elem` neighbours then EmptySeat else UsedSeat
       Floor -> Floor
       where neighbours = neighbourComputation pos g
             currentEntity = fromJust $ g !!? pos

roundtrip :: Grid -> ((Int,Int) -> Grid -> [RoomEntity]) -> Grid
roundtrip g neighbourComputation = V.imap (\i v -> V.imap (\j _ -> seatNextRound (i,j) g neighbourComputation) v) g

fixpoint :: Grid -> ((Int,Int) -> Grid -> [RoomEntity]) -> Int
fixpoint g neighbourComputation = let next = roundtrip g neighbourComputation in
        if next == g then
          getUsedSeats g
        else fixpoint next neighbourComputation

getUsedSeats :: Grid -> Int
getUsedSeats g = let rows = V.toList g in
     foldr (\e acc -> acc + (length . filter (UsedSeat ==) $ V.toList e)) 0 rows

parseRoom :: ReadP [[RoomEntity]]
parseRoom = parseRoomRow `endBy` newline

parseRoomRow :: ReadP [RoomEntity]
parseRoomRow = many (parseEmptySeat +++ parseUsedSeat +++ parseFloor)

parseUsedSeat :: ReadP RoomEntity
parseUsedSeat = char '#' $> UsedSeat

parseFloor :: ReadP RoomEntity
parseFloor = char '.' $> Floor

parseEmptySeat :: ReadP RoomEntity
parseEmptySeat = char 'L' $> EmptySeat

newline :: ReadP Char
newline = char '\n'

